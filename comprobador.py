#!/usr/bin/env python
# -*- coding: utf-8 -*-

print("La funcion siguiente entrega el promedio de numeros de una lista:")
def promedio(lista):
	#"len" devuelve la longitud de una cadena de caracteres o el número
    #de elementos de una lista
	a = len (lista)
	print("En total hay", a, "numeros en la cadena")
	#"sum" suma todos los valores presentes en la lista
	b = sum(lista)
	print("La suma de los numeros de la cadena es: ", b)
	#la siguiente operación es usada para calcular el promedio de los
	#datos de la lista, c toma los datos de b (suma de los valores) y los
	#divide en a (longitud de la cadena) obteniendo asi el promedio
	c = b / a
	print("El promedio de los numeros de la cadena es: ", c)
	return a, b, c
#"lista" contiene los valores de la lista
lista = [2,4,6,5,3,2,9]
#prom llama a la funcion promedio, para luego imprimirla
prom = promedio(lista) 
print(prom) 
print("\n")
print("El siguiente programa contará las letras de palabras ingresadas")
def cuenta_letras(lista):
	a = len(lista)
	print("Se ingresaron", a, "palabras")
	b = len ('buenas')
	print("La primera palabra de la lista tiene", b, "letras")
	c = len('tardes')
	print("La segunda palabra de la lista tiene", c, "letras")
	d = len('como')
	print("La tercera palabra de la lista tiene", d, "letras")
	e = len('estas')
	print("La cuarta palabra de la lista tiene", e, "letras")
	return b, c, d, e
	#len cuenta las letras de cada palabra, lo que luego será usado para 
	#ver cual de aquellas palabras es más larga
lista =  ['buenas', 'tardes', 'como', 'estas']
#cant_letras llama a la funcion cuenta_letras(lista)
cant_letras = cuenta_letras(lista)
print(cant_letras)

print("El siguiente programa entregara una lista en cuadrados")
def cuadrados(listas):
	a = (listas)
	elevado = a ** 2
	return elevado

listas = [2, 3, 5, 8, 9]
ele = cuadrados(listas)
print(ele)

	
